const { EleventyI18nPlugin } = require("@11ty/eleventy");
const i18n = require("eleventy-plugin-i18n");
const translations = require("./src/_data/i18n");

module.exports = function (eleventyConfig) {
  eleventyConfig.addPassthroughCopy("src/assets/");
  eleventyConfig.addPlugin(EleventyI18nPlugin, {
    defaultLanguage: "fr",
  });
  eleventyConfig.addPlugin(i18n, {
    translations,
    fallbackLocales: {
      "*": "fr",
    },
  });
  eleventyConfig.setBrowserSyncConfig({
    files: "./dist/assets/css/*.css",
  });
  eleventyConfig.addCollection("questions_en", function (collection) {
    return collection.getFilteredByGlob("./src/en/questions/*.njk");
  });
  eleventyConfig.addCollection("questions_fr", function (collection) {
    return collection.getFilteredByGlob("./src/fr/questions/*.njk");
  });
  eleventyConfig.addCollection("regulations_en", function (collection) {
    return collection.getFilteredByGlob("./src/en/regulations/*.njk");
  });
  eleventyConfig.addCollection("regulations_fr", function (collection) {
    return collection.getFilteredByGlob("./src/fr/regulations/*.njk");
  });
  eleventyConfig.addCollection("contents_fr", function (collection) {
    return collection.getFilteredByGlob("./src/fr/contents/*.md");
  });
  eleventyConfig.addCollection("contents_en", function (collection) {
    return collection.getFilteredByGlob("./src/en/contents/*.md");
  });
  return {
    dir: {
      input: "src",
      output: "dist",
      includes: "_includes",
    },
    templateFormats: ["njk", "html", "md"],
  };
};
