---
tags: content
fileSlug: legals
---

## Editorial information

The website of the National Travel Data Agency (ANDV) is edited and monitored by the ANDV.
Address:
Place Beauvau
75800 Paris Cedex 08

## Publication director

Préfet Seymour Morsy, Director of the ANDV

## Editor-in-chief

Bruno Roussel, Secretary General of the ANDV

## Hosting

The website is hosted on Scalingo’s cloud.

## Copyright and property rights

### Online content:

Information uploaded on the website pnr.gouv.fr are public and not covered by any property rights (art. L.122-5 of the Intellectual Property Code); they may be reproduced freely, under three conditions:

- The respect of the integrity of the reproduced information;
- The quotation of the source;
- The mention under which reproduction rights are reserved and strictly limited.

Any use for commercial or advertising purposes is prohibited.

### Graphic design – Pictures credits:

Pictures credits: ANDV
All image processing actions (framing, editing, retouching…) are prohibited without the agreement of the ANDV. The user abstains from any use contrary to the laws and regulations or threatening public order and from diverting the use of the pictures from their context, namely information purposes.
The user agrees to respect the moral rights of the photographers.
If these obligations are not met by the Internet user, the ANDV will institute legal proceedings.

For any question, you can contact the ANDV:

- By mail: ANDV – Place Beauvau – 78500 Paris Cedex 08
- By email: If you are a passenger **andv-voyageur@interieur.gouv.fr**, if you are an air carrier **relations-compagnies@uip.pnr.gouv.fr**

### Creation of the link to pnr.gouv.fr

The implementation of a link to the pnr.gouv.fr website is not, in principle, conditioned to any prior agreement provided that the purpose of the concerned websites is consistent with the purpose of the website of the ANDV.
However, the ANDV prohibits websites whose purposes would prove to be illegal or not compliant with the purpose of the ANDV website by diffusing, among others, information with racist, pornographic, xenophobic, homophobic characteristics or which is likely to offend the public, from creating such a link from their webpage.
It is strongly recommended that the title of the ANDV website be explicitly mentioned in the title of the link. It is strongly recommended that this webpage be opened in a separate browser window.

### Website content

#### Warning

Despite all the care taken in writing and putting documents online, typographical errors or technical inaccuracies cannot be excluded. The ANDV reserves the right to correct them at any time as soon as they are brought to its attention.
Information and documents available on the pnr.gouv.fr website may be updated at any time. Notably, they may be updated between the time it is downloaded by the user and the moment he/she reads the document.

#### Links

Links to other websites, public or private, may be offered to you on the pnr.gouv.fr website, in order to facilitate the access to information for the users.
These websites’ content does not engage the responsibility of the ANDV.

#### Consultation and downloading of documents

The ANDV distributes its publications in a PDF format.
Le Référentiel Général d’Interopérabilité V 2.0 , currently in force for French administrations, states that PDF is a page description language, whose specificity is to preserve the layout of a file – fonts, images, graphic objects, etc. – as it has been defined by its author, and this no matter the software, the operating system and the used material to print or visualize it. Version 1.7 of the standard is recommended.
Furthermore, the repository states that the PDF/A-1 is a standardised version of PDF. Its use is widely spread to conserve or exchange digital documents on the long-term. The PDF/A-1 format is true to original documents: fonts, images, graphic objects and the layout of the source file are kept, no matter the app used and the platform used to create it.
To read PDF format, several open-source and free readers are available. The website http://pdfreaders.org/ lists them, according to the used operating system (Windows, Mac OS, …).

### Website access

The ANDV as well as the web host strive to ensure that users have continuous access to the pnr.gouv.fr website.
Nonetheless, the ANDV reserves the right not to guarantee the access to the website in case of force majeure (breakdown, technical intention of maintenance). The communication department cannot be held responsible.
