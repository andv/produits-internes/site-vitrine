---
tags: content
fileSlug: data
---

The following section describes how the ANDV collects and processes the personal data of its visitors and users during their visit on the website pnr.gouv.fr (the aforementioned “website”).

## Principle and legal framework

If the ANDV collects and processes users’ personal data, it must do so with transparency, confidentiality and security, in respect with the European and French regulations that may be applied to it, namely:

- Regulation (EU) 2016/679 of the European Parliament of the 27th April 2016;
- Law n°78-17 of the 6th January 1978 on data processing, files and freedoms (“Data Protection and Freedoms Acts”), as amended.

## Origins of the collected personal data

No personal data is collected directly or indirectly during the browsing.
