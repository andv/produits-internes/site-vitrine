---
tags: content
fileSlug: accessibility
---

## Conformity declaration – RGAA 4.0

An audit of the pnr.gouv.fr will be carried out soon. Corrections will be made if necessary.

## Right to compensation

Pending full compliance, you can obtain an accessible version of the documents published on pnr.gouv.fr or of the contained information by sending an email to [email address to be defined], indicating the name of the concerned document and/or the information that you would like to obtain. The requested information will be sent to you as soon as possible.

## Improvement and contact

You can help us improving the accessibility of the website by reporting the potential issues that you encounter. To do so, please send us an email [email address to be defined].

## Défenseur des droits (Rights defender)

If you notice a lack of accessibility that could prevent you from accessing the website’s content or a functionality, and you have warned us about it and you do not manage to obtain a quick answer from us, you have the right to address your grievance or request for referral:

- By post, free of charge, without postage to: Défenseur des droits – Libre réponse 71120 75342 Paris CEDEX 07
- Online : https://www.defenseurdesdroits.fr/fr/contactez-nous
