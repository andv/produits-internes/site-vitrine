---
tags: content
fileSlug: accessibility
---

## Déclaration de conformité – RGAA 4.0

Un audit du site pnr.gouv.fr sera réalisé prochainement. Des corrections y seront apportées si nécessaire.

## Droit à la compensation

Dans l’attente d’une mise en conformité totale, vous pouvez obtenir une version accessible des documents publiés sur pnr.gouv.fr ou des informations qui y seraient contenues en envoyant un courriel à [adresse mail à définir] en indiquant le nom du document concerné et/ou les informations que vous souhaitez obtenir. Les informations demandées vous seront transmises dans les plus brefs délais.

## Amélioration et contact

Vous pouvez nous aider à améliorer l’accessibilité du site en nous signalant les problèmes éventuels que vous rencontrez. Pour ce faire, envoyez-nous un courriel [adresse mail à définir].

## Défenseur des droits

Si vous constatez un défaut d’accessibilité vous empêchant d’accéder à un contenu ou une fonctionnalité du site, et si vous nous le signalez mais ne parvenez pas à obtenir une réponse rapide de notre part, vous êtes en droit de faire parvenir vos doléances ou demandes de saisine :

- Par courrier gratuit, sans affranchissement à : Défenseur des droits – Libre réponse 71120 75342 Paris CEDEX 07
- En ligne : https://www.defenseurdesdroits.fr/fr/contactez-nous
