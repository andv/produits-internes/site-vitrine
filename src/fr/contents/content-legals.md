---
tags: content
fileSlug: legals
---

## Informations éditeur

Le site de l’agence nationale des données de voyage (ANDV) est édité et géré par l’ANDV.
Adresse :  
Place Beauvau
75800 Paris Cedex 08

## Directeur de publication

Le Préfet Seymour Morsy, directeur de l’ANDV

## Rédacteur en chef

Bruno Roussel, secrétaire général de l’ANDV

## Hébergement

Site hébergé sur le Cloud de Scalingo

## Copyright et droits d’auteur

### Reprise du contenu mis en ligne

Les informations mises en ligne sur le site pnr.gouv.fr sont publiques et ne sont couvertes par aucun droit d’auteur (art. L. 122-5 du Code de la propriété intellectuelle) ; elles peuvent être reproduites librement, sous trois conditions :

- Le respect de l’intégrité de l’information reproduite ;
- La citation de la source ;
- La mention selon laquelle les droits de reproduction sont réservés et strictement limités.

Toute utilisation à des fins commerciale ou publicitaire est interdite.

### Créations graphiques – Crédits photographiques

Crédits photos : ANDV.
Toute manipulation des images (recadrage, retouche, montage, trucage…) est interdite sans l’accord de l’ANDV.
L’internaute s’abstient de tout usage contraire aux lois et règlements ou portant atteinte à l’ordre public et de détourner l’utilisation des photographies de leur contexte d’actualité à savoir l’information.
L’internaute s’engage à respecter le droit moral des photographes.
Dans le cas où ces obligations ne seraient pas respectées par l’internaute, l’ANDV engagera des poursuites judiciaires.
Pour toute question vous pouvez contacter l’ANDV :

- Par courrier : ANDV – Place Beauvau – 75800 Paris Cedex 08
- Par courriel : Si vous êtes un voyageur **andv-voyageur@interieur.gouv.fr**, si vous êtes un transporteur **relations-compagnies@uip.pnr.gouv.fr**

### Création de liens vers pnr.gouv.fr

La mise en place de lien vers le site pnr.gouv.fr n’est conditionnée en principe à aucun accord préalable dans la mesure où l’objet des sites concernés est conforme à l’objet du site de l’ANDV.
Toutefois l’ANDV interdit aux sites dont l’objet s’avérerait illicite ou non conforme à celui du site pnr.gouv.fr en diffusant entre autres des informations à caractère raciste, pornographique, xénophobe, homophobe ou étant de nature à heurter la sensibilité du public, à créer un tel lien depuis leur page web.
La mention explicite de l’intitulé du site de l’ANDV dans l’intitulé du lien est vivement souhaitée. Il est fortement recommandé que l’ouverture de cette page se fasse dans une fenêtre indépendante du navigateur.

### Contenu du site

#### Avertissement

Malgré tout le soin apporté à la rédaction et la mise en ligne des documents, des erreurs typographiques ou des inexactitudes techniques ne peuvent être exclues. L’ANDV se réserve le droit de les corriger à tout moment dès qu’elles sont portées à sa connaissance.
Les informations et les documents contenus sur le site pnr.gouv.fr sont susceptibles de faire l’objet de mises à jour à tout moment. Notamment, elles peuvent faire l’objet de mises à jour entre le moment de leur téléchargement par l’internaute et celui où il en prend connaissance.

#### Liens

Des liens vers d’autres sites, publics ou privés peuvent vous être proposés sur le site pnr.gouv.fr afin de faciliter l’accès à l’information de l’internaute.
Le contenu de ces sites n’engage pas la responsabilité de l’ANDV.

#### Consultation et téléchargement des documents

L’ANDV diffuse ses publications au format PDF.
Le Référentiel Général d’Interopérabilité V 2.0 en vigueur pour les administrations françaises précise que le PDF est un langage de description de pages dont la spécificité est de préserver la mise en forme d’un fichier – polices d’écritures, images, objets graphiques, etc. – telle qu’elle a été définie par son auteur, et cela quels que soient le logiciel, le système d’exploitation et le matériel utilisés pour l’imprimer ou le visualiser. La version 1.7 du standard est recommandé.
Ce référentiel dispose en outre que le PDF/A-1 est une version standardisée du PDF. Son usage est très répandu pour conserver et échanger des documents numériques, sur le long terme. Le format PDF/A-1 est fidèle aux documents originaux : les polices, les images, les objets graphiques et la mise en forme du fichier source sont préservés, quelles que soient l’application et la plate-forme utilisées pour le créer.
Pour lire le format PDF, plusieurs lecteurs libres et gratuits existent. Le site http://pdfreaders.org/ les recense en fonction du système d’exploitation que vous utilisez (Windows, Mac OS,…).

### Accès au site

L’ANDV ainsi que l’hébergeur s’efforcent de permettre que les internautes puissent avoir accès en continu au site pnr.gouv.fr.
Néanmoins, l’ANDV se réserve le droit de ne pouvoir garantir l’accessibilité à son site internet en cas de force majeure (panne, intention technique de maintenance). La responsabilité du service communication ne pourrait être engagée.
