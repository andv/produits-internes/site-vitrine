---
tags: content
fileSlug: data
---

La présente section décrit comment l’ANDV collecte et traite les données personnelles des visiteurs et des usagers (ci-après les « usagers ») lors de leur navigation sur le site pnr.gouv.fr.

## Principe et cadre juridique

Si l’ANDV collecte et traite les données personnelles des usagers, elle doit le faire avec transparence, confidentialité et sécurité, dans le respect des réglementations européennes et françaises qui lui sont applicables, à savoir :

- Le règlement (UE) 2016/679 du Parlement européen du 27 avril 2016 ;
- La loi n°78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés (« loi informatique et libertés »), modifiée.

## Origine des données personnelles collectées

Il n’y a pas de données personnelles collectées directement ou indirectement lors de la navigation sur le site pnr.gouv.fr.
