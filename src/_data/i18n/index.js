module.exports = {
  faq: {
    fr: "Foire Aux Questions",
    en: "Frequently Asked Questions",
  },
  displaySettings: {
    fr: "Paramètres d'affichage",
    en: "Display settings",
  },
  chooseSettings: {
    fr: "Choisissez un thème pour personnaliser l’apparence du site.",
    en: "Choose a theme to customize the look of the site.",
  },
  lightMode: {
    fr: "Thème clair",
    en: "Light mode",
  },
  darkMode: {
    fr: "Thème sombre",
    en: "Dark mode",
  },
  systemMode: {
    fr: "Système",
    en: "System",
  },
  systemModeIndication: {
    fr: "Utilise les paramètres système.",
    en: "Use system settings.",
  },
  home: {
    fr: "Accueil",
    en: "Home",
  },
  toTop: {
    fr: "Haut de page",
    en: "Back to top",
  },
  breadcrumb: {
    fr: "Voir le fil d’Ariane",
    en: "View breadcrumb",
  },
  source: {
    fr: "Source image",
    en: "Image source",
  },
  more: {
    fr: "Pour en savoir plus :",
    en: "To learn more:",
  },
  close: {
    fr: "Fermer",
    en: "Close",
  },
  andvPresentation: {
    fr: "L’agence nationale des données de voyage est un service interministériel à compétence nationale rattaché au ministère de l’Intérieur et des Outre-mer.",
    en: "The National Travel Data Agency is an interministerial service with national competence attached to the Ministry of the Interior and Overseas Territories.",
  },
  partnersUE: {
    fr: "Financé par l’Union Européenne",
    en: "Funded by the European Union",
  },
  UEFlag: {
    fr: "Drapeau de l'Union Européenne",
    en: "European Union Flag",
  },
  a11y: {
    fr: "Accessibilité : non conforme",
    en: "Accessibility: non-compliant",
  },
  legals: {
    fr: "Mentions légales",
    en: "Legal notice",
  },
  data: {
    fr: "Protection des données",
    en: "Data protection page",
  },
  mention: {
    fr: "Sauf mention contraire, tous les contenus de ce site sont sous",
    en: "Unless otherwise stated, all content on this site is under",
  },
  about: {
    main: {
      fr: "Qui sommes-nous ?",
      en: "Who are we?",
    },
    anchors: [
      {
        fr: "Présentation de l’ANDV",
        en: "Presentation of the ANDV",
      },
      {
        fr: "Présentation du programme PNR",
        en: "Presentation of the “API-PNR France system”",
      },
      {
        fr: "Réglementation applicable API-PNR",
        en: "Regulation on API-PNR France",
      },
    ],
  },
  passenger: {
    main: {
      fr: "Je suis un voyageur",
      en: "I am a passenger",
    },
    anchors: [
      {
        fr: "Mes données",
        en: "My data",
      },
      {
        fr: "Mes droits",
        en: "My rights",
      },
      {
        fr: "Formulaire de contact voyageurs",
        en: "Contact form for passengers",
      },
    ],
  },
  carrier: {
    main: {
      fr: "Je suis un transporteur",
      en: "I am an air carrier",
    },
    anchors: [
      {
        fr: "Mes obligations",
        en: "My obligations",
      },
      {
        fr: "Je souhaite obtenir un certificat",
        en: "I want to be certified",
      },
      {
        fr: "Documentation technique",
        en: "Technical documentation",
      },
      {
        fr: "Contact form for carrier",
        en: "Contact form",
      },
    ],
  },
  navigation: {
    fr: "Menu",
    en: "Nav"
  }
};
