# Refonte site vitrine ANDV x DSFR

**Technologies :**
- Générateur de sites statics [Elevently](https://www.11ty.dev/)
- Hébergement [Scalingo](https://dashboard.scalingo.com)


**Accès rapides :**  

[🎨 Prototype Figma](https://www.figma.com/proto/LI1SD3bGhZb93jGMVJBbvl/ANDV?page-id=0%3A1&node-id=125%3A111849&viewport=538%2C236%2C0.06&scaling=min-zoom&starting-point-node-id=127%3A117215)  
[🤹‍♀️ Staging]()   
[🚀 Prod]()  


**Architecture :**

∟ 📄 .eleventy.js : fichier de configuration  
∟ 🗂 src : dossier source   
∟ 🗂 dist : dossier build
